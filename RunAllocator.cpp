// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream> // cin, cout
#include <string>   // getline, string
#include <vector>
#include <algorithm>    // std::sort


#include "Allocator.hpp"

using namespace std;

// ---------------
// allocator_solve
// ---------------
void allocator_solve(istream& sin, ostream& sout) {
    string s;
    int test_cases;
    sin >> test_cases;

    using pointer = double*;

    assert(test_cases > 0 && test_cases <= 100);
    getline(sin, s);
    getline(sin, s);
    for (int i = 0; i < test_cases; ++i) {
        using allocator_type = my_allocator<double, 1000>;
        allocator_type x;
        vector<pointer> v;

        while(sin.peek() != EOF && sin.peek() != '\n') {
            int num;
            sin >> num;

            if (num > 0) {
                // TODO: linked list of pointers
                pointer p = x.allocate(num);
                v.push_back(p);
            } else {
                // TODO: iterate until we find the block
                //only need to sort vector when dellocating
                sort(v.begin(), v.end());
                int index = abs(num) - 1;
                pointer dealloc = v.at(index);
                v.erase(v.begin() + index);
                // TODO: change -1 parameter later
                x.deallocate(dealloc, -1);
            }
            getline(sin, s);
        }
        getline(sin, s);

        // iterate over allocator and print sentinels
        sort(v.begin(), v.end());
        auto b = x.begin();
        auto e = x.end();
        while(b != e) {
            int val = *b;
            sout << val;
            b++; // TODO
            if (b != e) {
                sout << " ";
            }
        }
        sout << "\n";
    }
}

// ----
// main
// ----
int main () {
    using namespace std;
    allocator_solve(cin, cout);
    return 0;
}
