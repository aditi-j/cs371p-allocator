// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {

        // -----------
        // operator ==
        // -----------

        /**
         * Checking for equality between my_allcoator iterators
         *
         * @param i1 A reference to the first const iterator 
         * @param i2 A reference to the second const iterator 
         * @return is a boolean value
         */
        friend bool operator == (const iterator& i1, const iterator& i2) {
            return i1._p == i2._p;
        }

        // -----------
        // operator !=
        // -----------
        
        /**
         * Checking for non-equality between my_allcoator iterators
         *
         * @param i1 A reference to the first const iterator 
         * @param i2 A reference to the second const iterator 
         * @return is a boolean value
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;
        int sentinel_size = sizeof(int);

    public:
        // -----------
        // constructor
        // -----------

        /**
         * Constructing a my_allocator iterator 
         *
         * @param p An int pointer referring to the location of the iterator in memory
         */
        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        /**
         * Dereferencing the iterator, returning a reference to an int 
         * @return is an int*
         */
        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------
        
        /**
         * Advancing the iterator by block_size
         * @return a reference to the iterator 
         *
         */
        iterator& operator ++ () {
            int payload_size = *_p;
            int block_size = std::abs(payload_size) + 2 * sentinel_size;
            _p = (int *)((char *)_p + block_size); //reg arith
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Incrementing the value at the iterator 
         * @return a reference to the iterator 
         *
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrementing the iterator by block_size
         * @return a reference to the iterator 
         *
         */
        iterator& operator -- () {
            int payload_size = *_p;
            int block_size = std::abs(payload_size) + 2 * sentinel_size;
            _p = (int *)((char *)_p - block_size); //reg arith
            return *this;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrementing the value at the iterator 
         * @return a reference to the iterator 
         *
         */
        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * Checking for equality between my_allcoator const_iterators
         *
         * @param i1 A reference to the first const iterator 
         * @param i2 A reference to the second const iterator 
         * @return is a boolean value
         */
        friend bool operator == (const const_iterator& i1, const const_iterator& i2) {
            return i1._p == i2._p;
        }

        // -----------
        // operator !=
        // -----------
        
        /**
         * Checking for non-equality between my_allcoator const_iterators
         *
         * @param i1 A reference to the first const iterator 
         * @param i2 A reference to the second const iterator 
         * @return is a boolean value
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;
        int sentinel_size = sizeof(int);

    public:
        // -----------
        // constructor
        // -----------

        /**
         * Constructing a my_allocator const_iterator 
         *
         * @param p An int pointer referring to the location of the iterator in memory
         */
        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        /**
         * Dereferencing the const_iterator, returning a reference to an int 
         * @return is an int*
         */
        const int& operator * () const {
            return *_p;
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        /**
         * Incrementing the const_iterator 
         * @return a reference to the const_iterator 
         *
         */
        const_iterator& operator ++ () {
            int payload_size = *_p;
            int block_size = std::abs(payload_size) + 2 * sentinel_size;
            _p = (int *)((char *)_p + block_size); //reg arith
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Incrementing the value at const_iterator 
         * @return a reference to the const_iterator 
         *
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrementing the const_iterator 
         * @return a reference to the const_iterator 
         *
         */
        const_iterator& operator -- () {
            int payload_size = *_p;
            int block_size = std::abs(payload_size) + 2 * sentinel_size;
            _p = (int *)((char *)_p - block_size); //reg arith
            return *this;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrementing the value at const_iterator 
         * @return a reference to the const_iterator 
         *
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N] = {0};
    int sentinel_size = sizeof(int);

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * 1. no free blocks escaped coalescing
     * 2. can get to end properly
     * @return a boolean value
     */
    bool valid () const {
        const_iterator b = begin();
        const_iterator e = end();
        bool free = false;
        while (b != e) {
            if (*b > 0) { // no 2 adjacent free blocks
                if(free) {
                    return false;
                } else {
                    free = true;
                }
            } else {
                free = false;
            }
            b++;
        }
        if (b != e) {
            return false;
        }
        //able to traverse through heap, also no missed coalesc
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // check if N is good size
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }

        size_type sentinel_size = sizeof(int);
        size_type payload_size = N - 2 * sentinel_size;
        int* begin = (int*)(&a[0]);
        *begin = payload_size;

        int* end = (int*)(&a[N - sentinel_size]);
        *end = payload_size;

        // TODO: fix valid
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param a size_type
     * @return a pointer
     */
    pointer allocate (size_type s) {
        assert((int) s <= 992);
        // write a simple heap runner that runs through every block
        int * header = (int *)(&a[0]);
        int * end = (int *)(&a[N-1]);

        int payload_needed = s * sizeof(value_type);
        while(header < end) {

            int payload_size = *header;
            int block_size = std::abs(payload_size) + 2 * sentinel_size;

            if ( payload_size >= payload_needed) { //we found a free block that works!

                if ((int)(payload_size - payload_needed) >= (int)(sizeof(value_type) + (2 * sentinel_size))) {  // we should split

                    // allocating new busy block
                    *header = -1 * payload_needed;
                    int * footer = (int *)((char*) header + (payload_needed + sentinel_size));
                    *footer = -1 * payload_needed;

                    //filling in new free block
                    int new_payload = payload_size - payload_needed - 2 * sentinel_size; //d2
                    int * header_next = (int *)((char*) footer + sentinel_size);
                    *header_next = new_payload; //should be a positive number, it is a free block
                    int * footer_next = (int *)((char*) header_next + (new_payload + sentinel_size));
                    *footer_next = new_payload;
                } else {  // no split, give the whole block
                    *header = -1 * payload_size;
                    int * footer = (int *)((char*) header + (payload_size + sentinel_size));
                    *footer = -1 * payload_size;
                }
                return (pointer)((char*) header + sentinel_size);
            }
            header = (int *)((char*) header + block_size); //reg arith
        }

        assert(valid());
        return nullptr;
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     * @param a pointer
     * @param a const_reference v
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * @param p is the pointer to the block we want to free
     * @param s is the number of elements that were allocated to that block
     */
    void deallocate (pointer p, size_type s) {
        assert(sizeof(p) == 8);
        s *= -1;
        int* header = (int *)((char*) p - sentinel_size); //reg arith'

        *header *= -1;
        size_type payload_size = *header; //payload_size is positive

        //finding the footer
        int* footer = (int *)((char*) header + (payload_size + sentinel_size));
        *footer *= -1;
        
        int block_size = payload_size + 2 * sentinel_size;
        int* start = (int *)(&a[0]);
        int* end = (int *)(&a[N-1]);

        //handling merging right block if necessary
        int* right_block = (int*)((char*) header + block_size); //pointer arithmetic 
        bool merge_right = right_block < end && *right_block > 0; //checking to see if value is +/-
        if (merge_right) {
            assert(merge_right);
            //calculating new size
            int new_size = payload_size + *right_block + 2 * sentinel_size;
            //setting our header to new size
            *header = new_size; //s1
            //navigating to the new footer
            int* new_footer = (int*)((char*) header + (new_size + sentinel_size));
            *new_footer = new_size; //s4
            payload_size = new_size;
        }

        //handling merging left block if necessary
        int* left_footer = (int*)((char*) header - sentinel_size); //reg arith
        int* left_block = (int*)((char*) left_footer - sentinel_size - (int)abs(*left_footer)); //reg arith
        bool merge_left = left_block >= start && *left_block > 0;
        if (merge_left) {
            assert(merge_left);
            //calculating new size
            int new_size = *left_block + payload_size + 2 * sentinel_size;
            //setting header of left block to new size
            *left_block = new_size;
            //calculating value of new footer
            int* new_footer = (int*)((char*) left_block + (new_size + sentinel_size));
            *new_footer = new_size;
        }
        assert((int)*header > 0);
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     * @param a pointer
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * @param an int
     * @return a reference to an int
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * @param an int
     * @return a reference to a const int
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     * @return an iterator
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * @return a const_iterator
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     * @return an iterator
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * @return const_iterator
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
