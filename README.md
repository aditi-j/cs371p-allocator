# CS371p: Object-Oriented Programming Allocator Repo

* Name: Aditi Jain, Srikar Ganti

* EID: aj29462, sg49799

* GitLab ID: aditi-j, srikarsganti

* HackerRank ID: aditiawe1312, srikarsganti

* Git SHA: a2e8b265f450ca465a90dc53f8a6b65022feb55b

* GitLab Pipelines: https://gitlab.com/aditi-j/cs371p-allocator/-/pipelines

* Estimated completion time: 15 hours

* Actual completion time: 11 hours

* Comments: This was good. Buckets and buckets of fun!
