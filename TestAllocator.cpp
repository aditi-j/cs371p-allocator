// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

// behavior of actual allocator
TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    ASSERT_NE(b, nullptr);
    if (b != nullptr) {
        pointer e = b + s; // end()
        pointer p = b; // begin()
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// testing using my_allocator
TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s); //40 bytes
    ASSERT_NE(b, nullptr);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p; //? is this ++ from the iterator?
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// splitting-only
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s); //40 bytes
    const pointer    c = x.allocate(s); //40 bytes
    ASSERT_NE(b, nullptr);
    ASSERT_NE(c, nullptr);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p; //? is this ++ from the iterator?
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
    if (c != nullptr) {
        pointer e = c + s;
        pointer p = c;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p; //? is this ++ from the iterator?
            }
        }
        catch (...) {
            while (c != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(c, s);
            throw;
        }
        ASSERT_EQ(std::count(c, e, v), ptrdiff_t(s));
        while (c != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(c, s);
    }
}

// test merging blocks left and right
TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const pointer    b = x.allocate(100); //400 bytes
    const pointer    c = x.allocate(50); //200 bytes
    ASSERT_NE(b, nullptr);
    ASSERT_NE(c, nullptr);
    x.deallocate(b, 100);
    x.deallocate(c, 50);

    const pointer    d = x.allocate(200); //800 bytes
    ASSERT_NE(d, nullptr);
}

// tests iterator ==
TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type   x;
    x.allocate(100); //400 bytes
    x.allocate(50); //200 bytes
    x.allocate(25); //200 bytes

    allocator_type::iterator b = x.begin();
    allocator_type::iterator e = x.end();
    while(b != e)
        b++;
    bool equal = b == e;
    ASSERT_EQ(equal, true);
}

// tests iterator !=
TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type   x;
    x.allocate(100); //400 bytes
    x.allocate(50); //200 bytes
    x.allocate(25); //200 bytes

    auto b = x.begin();
    auto e = x.end();
    bool equal = b == e;
    ASSERT_EQ(equal, false);
}

// tests iterator *
TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type   x;
    x.allocate(100); //400 bytes
    x.allocate(50); //200 bytes
    x.allocate(25); //200 bytes

    auto b = x.begin();
    ASSERT_EQ(*b, -400);
    b++;
    ASSERT_EQ(*b, -200);
    b++;
    ASSERT_EQ(*b, -100);
    b++;
    ASSERT_EQ(*b, 268);
}

// tests iterator ++
TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type   x;
    x.allocate(100); //400 bytes
    x.allocate(50); //200 bytes
    x.allocate(25); //200 bytes

    auto b = x.begin();
    auto e = x.end();
    while(b != e) {
        b++;
    }
    bool eq = b == e;
    ASSERT_EQ(eq, true);
}

// tests iterator --
TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type   x;
    x.allocate(100); //400 bytes
    x.allocate(50); //200 bytes
    x.allocate(25); //200 bytes

    auto b = x.begin();
    auto e = x.end();
    while(b != e) {
        e--;
    }
    bool eq = b == e;
    ASSERT_EQ(eq, true);
}

// tests const_iterator ==
TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000> const;

    allocator_type   x;

    allocator_type::const_iterator b = x.begin();
    allocator_type::const_iterator e = x.end();
    while(b != e)
        b++;
    bool equal = b == e;
    ASSERT_EQ(equal, true);
}

// tests const_iterator !=
TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000> const;

    allocator_type   x;

    allocator_type::const_iterator b = x.begin();
    allocator_type::const_iterator e = x.end();
    bool equal = b == e;
    ASSERT_EQ(equal, false);
}

// tests const iterator *
TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<int, 1000> const;

    allocator_type   x;
    allocator_type::const_iterator b = x.begin();

    ASSERT_EQ(*b, 992);
    b++;
}

// tests const iterator ++
TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<int, 1000> const;

    allocator_type   x;
    allocator_type::const_iterator b = x.begin();
    allocator_type::const_iterator e = x.begin();

    while(b != e)
        b++;

    bool eq = b == e;
    ASSERT_EQ(eq, true);
}

// tests const iterator --
TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<int, 1000> const;

    allocator_type   x;
    allocator_type::const_iterator b = x.begin();
    allocator_type::const_iterator e = x.begin();

    while(b != e)
        e--;

    bool eq = b == e;
    ASSERT_EQ(eq, true);
}

// checks allocator constructor
TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}

// checks const allocator constructor
TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}

// tests correct sentinel val after allocating
TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;
    x.allocate(10);
    x.allocate(10);                           // read/write
    ASSERT_EQ(x[0], -40);
}

// tests correct sentinel val after deallocating
TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(10);
    pointer b = x.allocate(10);
    x.deallocate(b, 10);                      // read/write
    ASSERT_EQ(x[0], -40);
}

// tests if try to deallocate only part of a free block
TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(10);
    pointer b = x.allocate(10);
    x.deallocate(b, 5);                      // read/write
    ASSERT_EQ(x[0], -40);
}

// testing using my_allocator
TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<short, 1000>;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    size_type  s = 121;
    pointer a = x.allocate(s); //40 bytes
    ASSERT_NE(a, nullptr);
    for (int i = 0; i < 4; ++i) {
        x.allocate(s);
    }
    auto b = x.begin();
    auto e = x.end();
    while (b != e) {
        ASSERT_EQ(*b, -242);
        b++;
    }
}